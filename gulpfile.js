var lr = require('tiny-lr'), // Минивебсервер для livereload
    gulp = require('gulp'), // Сообственно Gulp JS
    livereload = require('gulp-livereload'), // Livereload для Gulp
    csso = require('gulp-csso'), // Минификация CSS
    htmlmin = require('gulp-htmlmin'), // Минификация HTML
    imagemin = require('gulp-imagemin'), // Минификация изображений
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    jshint = require('gulp-jshint'),
    jslint = require('gulp-jslint'),
    connect = require('connect'), // Webserver
    serveStatic = require('serve-static'),
    stylish = require('jshint-stylish');


gulp.task('html', function() {
    gulp.src(['./public/**/*.html'])
        .pipe(livereload()); // даем команду на перезагрузку страницы
});

gulp.task('template', function() {
    gulp.src(['./public/assets/js/templates/**/*.tmpl'])
        //.pipe(htmlmin())
        //.pipe(gulp.dest('./public/js/templates'))
        .pipe(livereload()); // даем команду на перезагрузку страницы
});

gulp.task('js', function() {
    gulp.src(['./public/assets/js/**/*.js', '!public/assets/js/lib/**/*'])
        //.pipe(gulp.dest('./public/js'))
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        /*.pipe(jslint({
            node: true,
            evil: false,
            nomen: true,
            global: [],
            predef: [],
            reporter: 'default',
            edition: '2014-07-08',
            errorsOnly: true
        })).on('error', function (error) {
            console.error(String(error));
        })*/
        .pipe(livereload()); // даем команду на перезагрузку страницы
});

gulp.task('bower', function() {
    gulp.src(['./bower_components/jquery/dist/jquery.js', 
                './bower_components/underscore/underscore.js',
                './bower_components/requirejs/require.js',
                './bower_components/requirejs-text/text.js',
                './bower_components/requirejs-plugins/src/async.js',
                './bower_components/requirejs-plugins/src/propertyParser.js',
                './bower_components/requirejs-plugins/src/goog.js',
                './bower_components/backbone/backbone.js',
                './bower_components/backbone.paginator/lib/backbone.paginator.js',
                './/bower_components/backbone.localStorage/backbone.localStorage.js',
                './bower_components/marionette/lib/backbone.marionette.js'])
        //.pipe(concat('lib.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('./public/assets/js/lib'));
});

gulp.task('css', function() {
    gulp.src(['./public/assets/css/**/*.css'])
        //.pipe(gulp.dest('./public/css'))
        .pipe(livereload()); // даем команду на перезагрузку страницы
});

gulp.task('images', function() {
    gulp.src('./public/assets/img/**/*')
        .pipe(imagemin())
        //.pipe(gulp.dest('./public/img'))

});

// Локальный сервер для разработки
gulp.task('http-server', function() {
    connect()
        .use(require('connect-livereload')())
        .use(serveStatic('./public'))
        .listen('9000', function () {
             console.log('Server listening on http://localhost:9000');
          });
});


// Запуск сервера разработки gulp watch
gulp.task('watch', function() {
        gulp.watch('public/**/*.html', ["html"]);
        gulp.watch('public/assets/js/templates/**/*.tmpl', ["template"]);
        gulp.watch('public/assets/css/**/*.css', ["css"]);
        gulp.watch('public/assets/js/**/*', ["js"]);
        livereload.listen();
});

gulp.task('default', ['http-server', 'bower','images','watch']);