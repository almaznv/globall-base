/*global define */
define([
    'underscore',
    'marionette'
], function (_, Marionette) {
    'use strict';

    var markerView = {
        default: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=0|CCCCCC|000000',
        over: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=0|00FF00|000000',
        selected: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=0|FF0000|000000',
        ingroup: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=0|FFFF00|000000'
    };

    return Marionette.Behavior.extend({
        ui: {},

        modelEvents: {
            "change:selected": "setSelected"
        },

        initialize: function(options) {
            var map, model;
            this.model = this.view.options.model;

            map = app.module("map");
            this.marker = map.view.setMarker({
                markerIcon: markerView.default,
                lat: this.model.get("lat"), 
                lon: this.model.get("lon"), 
                onClick: this.onClick(), 
                onMouseOver: this.onMouseOver(), 
                onMouseOut: this.onMouseOut()
            });

            this.on("SelectGroup", this.selectGroup);
            this.on("deSelectGroup", this.deSelectGroup);
        },

        selectGroup: function() {
            this.setMarkerIcon("ingroup");
            this.marker.ingroup = true;
        },

        deSelectGroup: function() {
            this.setMarkerIcon("default");
            this.marker.ingroup = false;
        },


        setSelected: function () {
            if (this.model.isSelected()) {
                this.setMarkerIcon('selected');
            } else {
                this.setMarkerIcon('default');
            }
        },

        setMarkerIcon: function(icon) {
            this.marker.setIcon(markerView[icon]);
        },

        onViewClick: function(event) {
            this.onClick({doNotFireViewEvent:true})(event);
        },

        onViewMouseOver: function(event) {
            this.onMouseOver({doNotFireViewEvent:true})(event);
        },

        onViewMouseOut: function(event) {
            this.onMouseOut({doNotFireViewEvent:true})(event);
        },

        onClick: function (options) {
            var s = this, doNotFireViewEvent;
            options = options || {};
            doNotFireViewEvent = options.doNotFireViewEvent;
            return function (event) {
                if (!doNotFireViewEvent) {
                    s.view.onClick(event);
                }
            };
        },

        onMouseOver: function (options) {
            var s = this, doNotFireViewEvent;
            options = options || {};
            doNotFireViewEvent = options.doNotFireViewEvent;
            return function (event) {
                s.marker.setIcon(markerView.over);
                if (!doNotFireViewEvent) {
                    s.view.onMouseOver(event);
                }
            };
        },

        onMouseOut: function (options) {
            var s = this, doNotFireViewEvent;
            options = options || {};
            
            doNotFireViewEvent = options.doNotFireViewEvent;
            return function (event) {
                
                if ( s.model.isSelected() ) {
                    s.marker.setIcon(markerView.selected);
                } else if (s.marker.ingroup) {
                    s.marker.setIcon(markerView.ingroup);
                } else {
                    s.marker.setIcon(markerView.default);
                }
                if (!doNotFireViewEvent) {
                    s.view.onMouseOut(event);
                }
            };
        },

        onBeforeDestroy: function (event) {
            this.marker.setMap(null);
        }
    });

});