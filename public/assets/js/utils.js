/*global define, window */
define([
        'jquery',
        'underscore'
        ], 
function ($, _) {
    "use strict";


    return {
        parseQueryString: function(queryString) {
            if (!_.isString(queryString)) {
                return;
            }
            queryString = queryString.substring( queryString.indexOf('?') + 1 );
            var params = {};
            var queryParts = decodeURI(queryString).split(/&/g);
            _.each(queryParts, function(val) {
                    var parts = val.split('=');
                    if (parts.length >= 1)
                    {
                        val = undefined;
                        if (parts.length == 2) {
                            val = parts[1];
                        }
                        params[parts[0]] = val;
                    }
                });
            return params;
        },

        toQueryString: function(queryParams) {
            var i = 0, queryString = '';
            _.each(queryParams, function(value, key) {
                if (i !== 0) {
                    queryString += '&';
                }
                queryString += key + "=" + value;
                i++;
            });
            if (queryString !== '') {
                queryString = '?' + queryString;
            }
            return queryString;
        }

    };

});