/*global define */
define(['marionette','routers/base'],function (Marionette, BaseRouter) {
    'use strict';
   
   return BaseRouter.extend({
        appRoutes: {
            "": "welcome",
            '*notFound': 'notFound'
        },

        onRoute: function(name, path, args){
            BaseRouter.prototype.onRoute.call(this, name, path, args);
            console.log("index route fired: " + name, path, args);
        }
    });
});