/*global define */
define(['app', 'marionette', 'utils'],function (app, Marionette, utils) {
    'use strict';
   
   return Marionette.AppRouter.extend({
        onRoute: function(name, path, args){
            var id = args[0], 
                queryString = args[1];

            var pattern = /\((.*)\)+/ig;
            app.currentBaseUrl = path.replace(pattern, "");
            app.currentQueryId = id;
            app.currentQueryParams = utils.parseQueryString(queryString);
        }
    });
});