/*global define */
define(['marionette','routers/base'],function (Marionette, BaseRouter) {
    'use strict';
   
   return BaseRouter.extend({
        appRoutes: {
          "search/filtered(/:id)(\?:params)": "getFiltered",
          "search/subscriptions(/:id)(\?:params)": "getSubscriptions",
          "search/favourites(/:id)(\?:params)": "getFavourites",
          "search/photoreportss(/:id)(\?:params)": "getPhotoReport",
        },
        
        onRoute: function(name, path, args){
            BaseRouter.prototype.onRoute.call(this, name, path, args);
            console.log("search route fired ", name, path, args);
        }
    });
});