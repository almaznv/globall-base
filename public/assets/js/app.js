/*global define, window */
define([
        'marionette',
        'underscore',
        'modules/Map',
        'modules/NavBar',
        'modules/SideBar',
        'utils'
        ], 
function (Marionette, _, Map, NavBar, SideBar, utils) {
    "use strict";

    var app = new Marionette.Application();

    app.settings = {
        title: "GloballOutdoor"
    };

    app.pages  = {
        filtered: {
            title: "Поиск по параметрам",
            url: "search/filtered"
        },
        subscriptions: {
            title: "Подписки",
            url: "search/subscriptions"
        },
        favourites: {
            title: "Избранные",
            url: "search/favourites"
        }
    };

    app.currentBaseUrl = undefined;
    app.currentQueryId = undefined;
    app.currentQueryParams = undefined;



    //изменение заголовка страницы
    app.vent.on("changeTitle", function(){
        var title, titleParts, sidebar;
        titleParts = _.filter(arguments, function(item){
            return (item);
        });
        titleParts.unshift(app.settings.title);
        title = titleParts.join(" - ");
        app.sidebar.vent.trigger("setTitle", arguments[0]);
        document.title = title;
    });

    app.vent.on("goTo", function(baseUrl, id, params){
        var url = '', 
            queryString,
            reserved;
        //console.log(baseUrl, id, params);
        if (params && app.currentQueryParams) {
            reserved = _.pick(app.currentQueryParams, 'page', 'ps');
            params = _.extend(params, reserved);
        }
        baseUrl = baseUrl || '';
        url += baseUrl;
        queryString = utils.toQueryString(params);
        if (id) {
            url += '/' + id;
        }
        if (params && _.keys(params).length) {
            url += queryString;
        }
        app.indexRouter.navigate(url, true);
    });

    app.addInitializer(function(){
        console.log("app initialized");
        var map = app.module("map", Map);
        var navbar = app.module("navbar", NavBar);
        var sidebar = app.module("sidebar", SideBar);
    });

    return (window.app = app);

});