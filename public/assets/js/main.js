require.config({
    paths : {
        underscore : 'lib/underscore',
        backbone   : 'lib/backbone',
        marionette : 'lib/backbone.marionette',
        paginator  : 'lib/backbone.paginator',
        localStorage : 'lib/backbone.localStorage',
        jquery     : 'lib/jquery',
        async      : 'lib/async',
        propertyParser: 'lib/propertyParser'
    },
    shim : {
        'lib/backbone-localStorage' : ['backbone'],
        underscore : {
            exports : '_'
        },
        backbone : {
            exports : 'Backbone',
            deps : ['jquery', 'underscore']
        },
        marionette : {
            exports : 'Backbone.Marionette',
            deps : ['backbone']
        },
        localStorage : {
            deps : ['backbone']
        },
        paginator : {
            deps : ['backbone'],
            exports: 'Backbone.Paginator'
        },
    },
    deps : ['jquery', 'underscore']
});

require([ 'app',
          'marionette',
          'backbone',
          'routers/index',
          'controllers/index',
          'routers/search', 
          'controllers/search'
        ], 
function (app, Marionette, Backbone, IndexRouter, indexController, SearchRouter, searchController) {
    "use strict";
    
    //переопредляем загрузку шаблонов для совместимостис requirejs
    Marionette.TemplateCache.prototype.loadTemplate = function(templateId) {

        var template = templateId;

        if (!template || template.length === 0){
            var msg = "Could not find template: '" + templateId + "'";
            var err = new Error(msg);
            err.name = "NoTemplateError";
            throw err;
        }

        return template;
    };

    Marionette.Behaviors.behaviorsLookup = function() {
        return window.Behaviors;
    };

    app.start();

    app.indexRouter = new IndexRouter({
        controller : indexController
    });
    app.searchRouter = new SearchRouter({
        controller : searchController
    });

    Backbone.history.start();
});