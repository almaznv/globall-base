/*global define */
define(function(require){
    "use strict";
    return {
        newsItemView: require('lib/text!templates/newsItemView.tmpl'),
        newsListView: require('lib/text!templates/newsListView.tmpl'),
        billboardMarkerLayoutView: require('lib/text!templates/billboardMarkerLayoutView.tmpl'),
        billboardPageableCompositeView: require('lib/text!templates/billboardPageableCompositeView.tmpl'),
        billboardCompositeView: require('lib/text!templates/billboardCompositeView.tmpl'),
        billboardSideItemView: require('lib/text!templates/billboardSideItemView.tmpl'),
        billboardGroupMarkerLayoutView: require('lib/text!templates/billboardGroupMarkerLayoutView.tmpl'),
        billboardGroupListView: require('lib/text!templates/billboardGroupListView.tmpl'),
        searchExtended: require('lib/text!templates/searchExtended.tmpl')
    };
});