/*global define */
define([
    'jquery','underscore', 'marionette', 'backbone', 'views/MapView', 'lib/goog!maps,3'
], function ( $, _, Marionette, Backbone, MapView ) {
    'use strict';

    var DEFAULT_LOCATION = {lat : 57.1141114013806, lon : 65.5729353341469}; //Тюмень

    var geocoder = new google.maps.Geocoder();
    var representationInCities = [
        {
            id: 123123,
            name: "Тюмень",
            geoname: "город Тюмень, Тюменская область, Россия",
            lat: 57.1141114013806,
            lon: 65.5729353341469
        }
    ];

    google.maps.Map.prototype.panToWithOffset = function(latlng) {
        var map = this;
        var ov = new google.maps.OverlayView();
        ov.onAdd = function() {
            var proj = this.getProjection();
            var aPoint = new google.maps.Point(0,0);
            aPoint.x = $(window).width()/2 - $(window).width()/2.4;
            aPoint.y = $(window).height()/2;
            map.panTo(proj.fromContainerPixelToLatLng(aPoint));
        }; 
        ov.draw = function() {}; 
        ov.setMap(this); 
    };

    return Marionette.Module.extend({

        initialize: function (options) {
            var s = this;
            this.vent = new Backbone.Wreqr.EventAggregator();
             this.vent.on("panMarker", function(location , withOffset){
                s.panMarker(location, withOffset);
                s.addMapDelayedEvent(Object.getPrototypeOf(s).panMarker, [location, withOffset]);
            });
            this.vent.on("panGroup", function(locations, withOffset){
                s.panGroup(locations, withOffset);
                s.addMapDelayedEvent(Object.getPrototypeOf(s).panGroup, [locations, withOffset]);
            });

            this.mapDelayedEventsCollection = new Backbone.Collection();

        },

        onStart: function (options) {
            var location, localStorage;

            console.log("map module initialized");

            location = DEFAULT_LOCATION;
            localStorage = window.localStorage;

            //переходим к последней посещенной локации, если есть информация
            if ( localStorage.getItem("lastLocation") ) {
                location = JSON.parse( localStorage.getItem("lastLocation") );
            }

            //функция првоерки города предствительства TODO убрать при релизе
            this.checkRepresentationInCity(location);

            this.view = new MapView({location : location, mapDelayedEventsCollection: this.mapDelayedEventsCollection});

            google.maps.event.addDomListener(this.view.map, 'panGroupWithOffset', this.ppp);

            if ( !localStorage.getItem("lastLocation") ) {
                this.setUserLocation();
                if (this.userLocation) {
                    this.checkRepresentationInCity(location);
                }
            }

        },

        //Если доступна геолокация перемещаем карту к координатам пользователя
        setUserLocation: function() {
            var s = this;
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    s.view.setCenter(position.coords.latitude, position.coords.longitude);
                    s.userLocation = {
                        lat: position.coords.latitude,
                        lon: position.coords.longitude
                    };
                });
            }
            return s.userLocation;
        },

        //проверяем, есть ли представительство в городе по входящим координатам
        checkRepresentationInCity: function(location) {
            this.geocodeByLatLng(location.lat, location.lon, function (objects) {
                var city, cities = [];
                city = _.find(representationInCities, function (_city) {
                    return _.findWhere(objects, {formatted_address: _city.geoname});
                });
                if (!city) {
                    _.each(representationInCities, function(_city) {
                        cities.push("<a href='#?moveto="+_city.lat+","+_city.lon+"'>"+_city.name+"</a>");
                    });
                    //TODO  переделать во вслывающее окно
                    $("body").append("<div class='representationInfo'>К сожалению в данном регионе/городе нет нашего представительства. Наши города: "+cities.join(", ")+"</div>");
                }
            });
        },

        geocodeByLatLng: function(lat, lon, callback) {
            var latLng = new google.maps.LatLng(lat, lon);
            geocoder.geocode( {'latLng': latLng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    callback(results);
                } else {
                    console.log('Geocode was not successful for the following reason: ' + status);
                }
            });
        },

        geocodeByAddress: function(address, callback) {
            geocoder.geocode( {'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    callback(results);
                } else {
                    console.log('Geocode was not successful for the following reason: ' + status);
                }
            });
        },

        panMarker: function (location, withOffset) {
            app.map.view.panToMarker(location, withOffset);
        },

        panGroup: function (locations, withOffset) {
            app.map.view.panToGroupMarkers(locations, withOffset);
        },

        addMapDelayedEvent: function (func, args) {
            this.mapDelayedEventsCollection.add({func : func, args: args});
        }
    });
});