/*global define */
define(['backbone', 'marionette', 'views/SideBarView'
], function (Backbone, Marionette, SideBarView) {
    'use strict';

    return Marionette.Module.extend({
        initialize: function(options) {
            var s = this;
            console.log("sidebar module initialized");

            this.view = new SideBarView();
            this.vent = new Backbone.Wreqr.EventAggregator();

            this.vent.on("resetForm", function(params){
                s.view.resetSearchExtended();
                s.view.initializeSearchExtended(params, true);
            });
            this.vent.on("setTitle", function(text){
                s.view.setTitle(text);
            });
            this.vent.on("setResults", function(view){
                s.view.list.show(view);
            });
            this.vent.on("parseToExtendedParamsForm", function(queryParams){
                if (queryParams && queryParams.ps) {
                    s.view.initializeSearchExtended(queryParams);
                }
            });
        }
    });
});