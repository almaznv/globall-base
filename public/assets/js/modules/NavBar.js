/*global define */
define([
    'marionette', 'views/NavBarView'
], function (Marionette, NavBarView) {
    'use strict';

    return Marionette.Module.extend({
        onStart: function(options) {
            console.log("navbar module initialized");
            this.view = new NavBarView();
        }
    });
});