define(['collections/NewsList',
        'views/index/NewsListView'
        ], 
function (NewsList, NewsListView) {
    "use strict";

    return {
        initDefaults : function() {
            if (app.sidebar.view) {
                app.sidebar.view.list.empty();
                app.sidebar.view.search_extended.empty();
            }
        },

        welcome : function() {
            this.initDefaults();
            var sideBar, map, pageSettings;

            pageSettings = {
                title: "Главная",
                url: ""
            };

            sideBar = app.module("sidebar");
            map = app.module("map");

            var newsList = new NewsList();
            var newsListView = new NewsListView({collection: newsList});

            //newsList.fetch();

            sideBar.view.list.show(newsListView);

            app.vent.trigger("changeTitle", pageSettings.title);
        },

        notFound : function() {

        }
    };
});