define(['utils',
        'collections/BillboardPageableList',
        'views/search/BillboardPageableCompositeView',
        'collections/BillboardGroupList',
        'views/search/BillboardGroupCompositeView'
    ], 
function (utils, BillboardPageableList, BillboardPageableCompositeView, BillboardGroupList, BillboardGroupCompositeView) {
    "use strict";

    return {
        initDefaults : function() {
            if (app.sidebar.view) {
                app.sidebar.view.list.empty();
                app.sidebar.view.search_extended.empty();
            }
        },
        getFiltered : function (id, params) {
            this.initDefaults();

            var queryParams = utils.parseQueryString(params);

            var billboardList = new BillboardPageableList();
            var billboardPageableCompositeView = new BillboardPageableCompositeView({
                                                collection: billboardList, 
                                                queryParams: queryParams,
                                                url: app.pages.filtered.url,
                                                modelId: parseInt(id)
                                            });

            app.sidebar.vent.trigger("parseToExtendedParamsForm", queryParams);
            app.sidebar.vent.trigger("setResults", billboardPageableCompositeView);
            app.vent.trigger("changeTitle", app.pages.filtered.title);
        },
        getSubscriptions : function (id, params) {
            this.initDefaults();

            var queryParams = utils.parseQueryString(params);

            var billboardGroupList = new BillboardGroupList();
            var billboardGroupCompositeView = new BillboardGroupCompositeView({
                                                collection: billboardGroupList, 
                                                queryParams: queryParams,
                                                url: app.pages.subscriptions.url,
                                                modelId: parseInt(id)
                                            });

            app.sidebar.vent.trigger("parseToExtendedParamsForm", queryParams);
            app.sidebar.vent.trigger("setResults", billboardGroupCompositeView);
            app.vent.trigger("changeTitle", app.pages.subscriptions.title);

        },
        getFavourites : function (id, params) {
            this.initDefaults();

            var queryParams = utils.parseQueryString(params);

            var billboardList = new BillboardPageableList();
            var billboardPageableCompositeView = new BillboardPageableCompositeView({
                                                collection: billboardList, 
                                                queryParams: queryParams,
                                                url: app.pages.favourites.url,
                                                modelId: parseInt(id)
                                         });

            app.sidebar.vent.trigger("parseToExtendedParamsForm", queryParams);
            app.sidebar.vent.trigger("setResults", billboardPageableCompositeView);
            app.vent.trigger("changeTitle", app.pages.favourites.title);
        },

        getPhotoReport : function (id, params) {
            this.initDefaults();

        }
    };
});