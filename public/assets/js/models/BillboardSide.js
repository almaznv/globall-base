/*global define */
define([
    'backbone',
    'models/_Base'
], function (Backbone, Base) {
    'use strict';

    return Base.extend({
        localStorage: new Backbone.LocalStorage('billboardSide'),

        defaults: {
            favourite: false,
            subscribe: false
        },
    });
});