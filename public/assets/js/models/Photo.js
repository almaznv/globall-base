/*global define */
define([
    'backbone',
    'localStorage'
], function (Backbone) {
    'use strict';

    return Backbone.Model.extend({
        localStorage: new Backbone.LocalStorage('photo'),
    });
});