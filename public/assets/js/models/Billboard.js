/*global define */
define([
    'backbone',
    'models/_Base'
], function (Backbone, Base) {
    'use strict';

    return Base.extend({
        localStorage: new Backbone.LocalStorage('billboard'),

        initialize: function(){
            this.setLocation();
        },

        setLocation: function(){
            this.set("location", {lat: this.get("lat"), lon: this.get("lon")});
        }
    });
});