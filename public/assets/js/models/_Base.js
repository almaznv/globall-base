/*global define */
define([
    'backbone',
    'localStorage'
], function (Backbone) {
    'use strict';

    return Backbone.Model.extend({
        
        isSelected: function () {
            return this.get("selected") || false;
        },

        setSelected: function () {
            this.collection.setSelected(this);
        },

        deselect: function () {
            return this.set("selected", false);
        }
    });
});