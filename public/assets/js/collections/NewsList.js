/*global define */
define([
    'backbone',
    'models/News',
    'localStorage'
], function (Backbone, News) {
    'use strict';

    return Backbone.Collection.extend({
        model: News,

        localStorage: new Backbone.LocalStorage('news'),

        initialize: function(){
            this.newSeed();
        },

        newSeed: function(){
            //this.create({id:1, title: "Новость 1", text: "Статистика"});
            //this.create({id:2, title: "Новость 2", text: "Статистика"});
            //this.create({id:3, title: "Новость 3", text: "Статистика1"});
        }

    });
});