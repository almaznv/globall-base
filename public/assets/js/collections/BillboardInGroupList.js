/*global define */
define([
    'backbone',
    'collections/_BaseList',
    'models/BillboardInGroup',
    'localStorage'
], function (Backbone, BaseList, BillboardInGroup) {
    'use strict';

    /*
     { 
        id: <id>,
        billboard_id: <billboard_id>,
        group_id: <group_id>
     }
     */
    return BaseList.extend({
        model: BillboardInGroup,

        localStorage: new Backbone.LocalStorage('billboardInGroup'),

        initialize: function (options) {
            BaseList.prototype.initialize.call(this, options);
            this.newSeed();
        },

        newSeed: function(){
            //this.create({id: 111, billboard_id:123123, group_id: 1});
        }
    });
});