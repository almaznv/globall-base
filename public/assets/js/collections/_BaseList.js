/*global define */
define([
    'backbone'
], function (Backbone) {
    'use strict';

    return Backbone.Collection.extend({
        initialize: function(){
            this.selected = null;
        },

        setSelected: function(model) {
            if (this.selected) {
                this.selected.set({selected:false});
            }
            model.set({selected:true});
            this.selected = model;
        }
    });
    
});