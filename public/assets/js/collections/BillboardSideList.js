/*global define */
define([
    'backbone',
    'models/BillboardSide',
    'localStorage'
], function (Backbone, BillboardSide) {
    'use strict';

    return Backbone.Collection.extend({
        model: BillboardSide,

        localStorage: new Backbone.LocalStorage('billboardSide'),

        initialize: function(){
            this.newSeed();
        },

        newSeed: function(){
            //this.create( {id: 111, name : "Сторона А", favourite: true, subscribe: false} );
            //this.create( {id: 222, name : "Сторона F", favourite: false, subscribe: true} );
        }
    });
});