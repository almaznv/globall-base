/*global define */
define([
    'backbone', 'paginator'
], function (Backbone) {
    'use strict';

    return Backbone.PageableCollection.extend({
        mode: "client",

        initialize: function(){
            this.selected = null;
        },

        setSelected: function(model) {
            if (this.selected) {
                this.selected.set({selected:false});
            }
            model.set({selected:true});
            this.selected = model;
        },

        state: {
            firstPage: 1,
            pageSize: 2
        },

        queryParams: {
            currentPage: "current_page",
            pageSize: "page_size"
        }

    });
});