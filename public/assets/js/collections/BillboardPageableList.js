/*global define */
define([
    'backbone',
    'collections/_BasePageableList',
    'models/Billboard',
    'localStorage'
], function (Backbone, BasePageableList, Billboard) {
    'use strict';

        /*
     { 
        id: <id>,
        address: <address>,
        lat: <lat>,
        lon: <lon>
     }
     */
    return BasePageableList.extend({
        model: Billboard,

        localStorage: new Backbone.LocalStorage('billboard'),

        initialize: function(options){
            BasePageableList.prototype.initialize.call(this, options);
            this.newSeed();
        },

        newSeed: function(){
            //this.create({id: 123123, address : "1 50 лет ВЛКСМ 49", lat : 57.120911, lon : 65.581571});
            //this.create({id: 213, address : "2 Мельникайте", lat : 57.1207395, lon : 65.5811899});
            //this.create({id: 344, address : "3 Республики", lat : 57.140901, lon : 65.540828});
            //this.create({id: 678, address : "4 Ленина", lat : 57.124713, lon : 65.582466});
        }
    });
});