/*global define */
define([
	'backbone',
	'models/Photo',
	'localStorage'
], function (Backbone, Photo) {
	'use strict';

	return Backbone.Collection.extend({
		model: Photo,

		localStorage: new Backbone.LocalStorage('photo'),
	});
});