/*global define */
define([
	'backbone',
	'models/PhotoReport',
	'localStorage'
], function (Backbone, PhotoReport) {
	'use strict';

	return Backbone.Collection.extend({
		model: PhotoReport,

		localStorage: new Backbone.LocalStorage('photoReport'),
	});
});