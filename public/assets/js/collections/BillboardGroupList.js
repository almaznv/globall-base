/*global define */
define([
    'backbone',
    'collections/_BaseList',
    'models/BillboardGroup',
    'localStorage'
], function (Backbone, BaseList, BillboardGroup) {
    'use strict';

    /*
     { группы пользователя #user_id
        id: <id>,
        name: <name>,
     }
     */
    return BaseList.extend({
        model: BillboardGroup,

        localStorage: new Backbone.LocalStorage('billboardGroup'),

        initialize: function (options) {
            BaseList.prototype.initialize.call(this, options);
            this.newSeed();
            this.create({id: "no", name : 'Без группы'});
        },

        newSeed: function(){
            //this.create({id: 1, name : 'Группа "МТС"'});
            //this.create({id: 2, name : 'Группа "Коминтел"'});
            //this.create({id: 3, name : 'Группа "М-видео"'});
        }
    });
});