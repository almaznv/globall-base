/*global define */
define([
    'marionette',
    'views/SearchExtendedView'
], function (Marionette, SearchExtendedView) {
    'use strict';

    return Marionette.LayoutView.extend({
        el:"#m_sidebar",

        ui: {
            title: ".title",
            search_extended_button: ".search_extended_button .link"
        },

        regions: {
            search: "#me_search",
            list: "#me_list",
            search_extended: "#me_search_extended"
        },

        events: {
            'click @ui.search_extended_button': 'toggleSearchExtended'
        },

        initialize: function (options) {
            this.bindUIElements();
        },

        setTitle: function (text) {
            this.ui.title.text(text);
        },

        toggleSearchExtended: function() {
            if ( this.search_extended.hasView() ) {
                this.resetSearchExtended();
            } else {
                this.initializeSearchExtended();
            }
        },

        initializeSearchExtended: function(queryParams, noFiltered) {
            var searchExtendedView =  new SearchExtendedView({queryParams : queryParams, noFiltered: noFiltered});
            this.search_extended.show(searchExtendedView);
        },

        resetSearchExtended: function() {
            this.search_extended.reset();
        }
    });
    
});