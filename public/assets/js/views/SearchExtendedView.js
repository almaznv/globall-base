/*global define */
define([
    'underscore',
    'jquery',
    'marionette',
    'templates',
    'utils'
], function (_, $, Marionette, templates, utils) {
    'use strict';

    return Marionette.ItemView.extend({
        template: templates.searchExtended,

        ui: {
            submit : 'span.submit',
            form : 'form',
            filters : '.filters',
            filtered : '.already_filtered ul',
            filteredClear : '.already_filtered span'
        },

        events: {
            'click @ui.submit': 'onSubmit',
            'click @ui.filteredClear': 'onClearFiltered'
        },

        initialize: function (options) {
            options.queryParams = options.queryParams || {};
            this.queryParams = options.queryParams;
            this.noFiltered = options.noFiltered;
            var FormModel = Backbone.Model.extend({});
            this.model = new FormModel({v : options.queryParams});
        },

        onShow: function () {
            if (!this.noFiltered) {
                this.setAlreadyFiltered(this.queryParams);
            }
        },

        onSubmit: function () {
            var queryString = this.ui.form.serialize(),
                queryParams = utils.parseQueryString(queryString),
                currentBaseUrl = app.currentBaseUrl;

            //проверяем, можно ли применить фильтрацию в этом разделе или необходимо перейти к раздулеу с фильтрацией
            if ( _.intersection([app.currentBaseUrl], [app.pages.favourites.url, app.pages.filtered.url]).length === 0 ) {
                currentBaseUrl = app.pages.filtered.url;
            }
            app.vent.trigger('goTo', currentBaseUrl, null, queryParams);
        },

        onClearFiltered: function () {
            app.sidebar.vent.trigger('resetForm', this.queryParams);
        },

        setAlreadyFiltered: function (queryParams) {
            var s = this, ewrapper, element;
            _.each(queryParams, function (item, key) {
                if (!item) return;
                ewrapper = s.ui.form.find("#"+key).closest(".fe-wrapper");
                if (ewrapper.length) {
                    element = ewrapper.detach();
                    s.ui.filtered.append(element);
                    s.ui.filters.hide();
                    s.ui.filteredClear.show();
                }
            });
        }
    });
});