/*global define */
define(['marionette'

], function (Marionette) {
    'use strict';

    return Marionette.CompositeView.extend({

        childViewOptions: function() {
            return {
              url: this.url,
              queryParams: this.queryParams,
              modelId: this.modelId
            };
        },

        initialize: function(options) {
            _.extend(this, options);
            this.queryParams = this.queryParams || {};
            this.link = this.getLink();
        },

        onShow: function(event) {
            var model;
            if (this.modelId) {
                model = this.collection.findWhere({id: this.modelId});
                if (model) {
                    this.collection.setSelected(model);
                }
            }
        },

        getLink: function() {
            return {
                url: this.url,
                queryParams : this.queryParams,
            };
        }

    });
});