/*global define */
define([
    'views/search/_BaseMarkerLayoutView'
], function (BaseMarkerLayoutView) {
    'use strict';

    return BaseMarkerLayoutView.extend({
        /*behaviors: {
            Markers: {
                behaviorClass: Markers
            }
        },*/

        ui: function(){
            return _.extend({}, _.result(BaseMarkerLayoutView.prototype, 'ui') || {}, {
                deleteGroup: '.delete-group',
                head: '.group-head'
            });
        },

        events: function(){
            return _.extend({}, _.result(BaseMarkerLayoutView.prototype, 'events') || {}, {
                'click @ui.deleteGroup' : 'deleteGroup'
            });
        },

        initialize: function (options) {
            BaseMarkerLayoutView.prototype.initialize.call(this, options);
            //добавляем регион где будут отображаться элементы группы
            this.addRegions({
                childs: "#childs-group-"+this.model.id
            });
        },

        onShow: function(event) {
            //удаляем кнопку удаления  общей группы "Без группы"
            if (this.model.id === "no") {
                this.ui.deleteGroup.remove();
            }

            var s = this;
            //подгружаем элементы для группы
            this.childs.show( new this.ListInGroupView({
                                                            collection: this.collection,
                                                            url: this.url,
                                                            modelId: parseInt(this.modelInGroupId),
                                                            groupModelId: parseInt(this.modelId),
                                                            queryParams: this.queryParams,
                                                            groupId: this.model.id,
                                                            childViewOptions: function() {
                                                                return {
                                                                      url: this.url,
                                                                      queryParams: this.queryParams,
                                                                      modelId: this.modelId,
                                                                      groupId: s.model.id,
                                                                      getLink: function(){
                                                                        return {
                                                                               url: this.url,
                                                                               id: null,//this.groupId,
                                                                               queryParams : { id: parseInt(this.model.id) }
                                                                        };
                                                                      }
                                                                };
                                                            },
                                                        }));
        },

        panGroupOnMap: function() {
            if (!app.map) return;
            app.map.vent.trigger("panGroup", this.getLocations());
        },

        panGroupWithOffsetOnMap: function() {
            if (!app.map) return; 
            app.map.vent.trigger("panGroup", this.getLocations(), true);
        },

        getLocations: function() {
            var locations = [];
            this.collection.each( function (item) {
                locations.push(item.get('location'));
            });
            return locations;
        },

        selectGroup: function() {
            this.childs.currentView.children.each(function(item){
                item.triggerMethod("SelectGroup");
            });
        },

        deSelectGroup: function() {
            this.childs.currentView.children.each(function(item){
                item.triggerMethod("deSelectGroup");
            });
        },

        deleteGroup: function(event) {
            console.log("delete", this.model, this);
            if (this.model.id !== "no") {
                this.model.destroy();
            }
        },

        onClick: function(event, options) {
             //BaseGroupMarkerLayoutView.prototype.onClick.call(this, options);
            app.vent.trigger("goTo", this.url, this.model.id, null);
        },

        onSelected: function(options) {
            this.ui.head.css("background","purple");
        },

        onDeSelected: function(options) {
            this.ui.head.css("background","yellow");
        }

    });
});