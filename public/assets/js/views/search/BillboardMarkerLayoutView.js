/*global define */
define(['app',
    'marionette',
    'templates',
    'views/search/_BaseMarkerLayoutView',
    'collections/BillboardSideList',
    'views/search/BillboardSideCollectionView',
    'behaviors/Marker',
    'lib/goog!maps,3'

], function (app, Marionette, templates, BaseMarkerLayoutView, BillboardSideList, BillboardSideCollectionView, Marker) {
    'use strict';

    return BaseMarkerLayoutView.extend({
        tagName: 'li',

        template: templates.billboardMarkerLayoutView,

        /*ui: function(){
            return _.extend({}, _.result(BaseMarkerLayoutView.prototype, 'ui') || {}, {
              //'click' : 'onclickChild'
            });
        },

        events: function(){
            return _.extend({}, _.result(BaseMarkerLayoutView.prototype, 'events') || {}, {
              //'click' : 'onclickChild'
            });
        },

        modelEvents: function(){
            return _.extend({}, _.result(BaseMarkerLayoutView.prototype, 'modelEvents') || {}, {
              //'click' : 'onclickChild'
            });
        },

        behaviors: function(){
            return _.extend({}, _.result(BaseMarkerLayoutView.prototype, 'behaviors') || {}, {

            });
        },*/

        initialize: function (options) {
            BaseMarkerLayoutView.prototype.initialize.call(this, options);
            this.addRegions({
                sidelist: "#side-list-"+this.model.id
            });
        },

        onShow: function (options) {
            BaseMarkerLayoutView.prototype.initialize.call(this, options);
            if (this.modelId == this.model.id) {
                this.panMarkerOnMap();
            }
        },

        onClick: function(event) {
            //BaseMarkerLayoutView.prototype.onClick.call(this, event);
            if ( this.model.isSelected() ) { 
                app.vent.trigger("goTo", this.link.url, null, this.link.queryParams);
            } else {
                app.vent.trigger("goTo", this.link.url, this.link.id, this.link.queryParams);
            }
        },

        onSelected: function(event) {
            var billboardSideList = new BillboardSideList();
            var billboardSideCollectionView = new BillboardSideCollectionView({collection: billboardSideList});
            this.sidelist.show(billboardSideCollectionView);
        },

        onDeSelected: function(event) {
            this.sidelist.reset();
        }
    });
});