/*global define */
define([
    'views/search/_BaseCompositeView'
], function (BaseCompositeView) {
    'use strict';

    return BaseCompositeView.extend({

        ui: {
            addButton: ".add-group .link",
            groupName: ".add-group .group-name"
        },

        events: {
           "click @ui.addButton" : "addGroup"
        },

        childViewOptions: function () {
            return {
              url: this.url,
              queryParams: this.queryParams,
              modelId: this.modelId,
              modelInGroupId: this.modelInGroupId
            };
        },

        initialize: function (options) {
            BaseCompositeView.prototype.initialize.call(this, options);
            this.modelInGroupId = parseInt(this.queryParams.id);
            this.collection.fetch({data: this.queryParams});
        },

        addGroup: function (event) {

            var model, groupName = this.ui.groupName.val().trim();
            console.log(groupName, this);
            if (groupName) {
                this.collection.create({id: 4, name: groupName});
            }
            app.vent.trigger("goTo", this.url);
        }

    });
});