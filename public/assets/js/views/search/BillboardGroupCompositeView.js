/*global define */
define([
    'views/search/_BaseGroupCompositeView',
    'templates',
    'views/search/BillboardGroupMarkerLayoutView',
    'models/BillboardGroup'
], function (BaseGroupCompositeView,  templates, BillboardGroupMarkerLayoutView, BillboardGroupModel) {
    'use strict';

    return BaseGroupCompositeView.extend({
        template: templates.billboardGroupListView,
        childView: BillboardGroupMarkerLayoutView,
        childViewContainer : ".group-list",

        initialize: function (options) {
            BaseGroupCompositeView.prototype.initialize.call(this, options);
            this.bindUIElements();
            this.GroupModel = BillboardGroupModel;
        }

    });

});