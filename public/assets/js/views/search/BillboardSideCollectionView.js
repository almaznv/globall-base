/*global define */
define([
    'marionette',
    'templates',
    'views/search/_BaseCollectionView',
    'views/search/BillboardSideItemView'
], function (Marionette, templates, BaseCollectionView, BillboardSideItemView) {
    'use strict';

    return BaseCollectionView.extend({
        tagName: 'ul',

        childView: BillboardSideItemView,

        initialize: function(options) {
            BaseCollectionView.prototype.initialize.call(this, options);
            this.collection.fetch();
        }
    });

});