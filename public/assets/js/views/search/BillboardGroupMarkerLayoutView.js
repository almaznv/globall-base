/*global define */
define(['marionette',
    'templates',
    'views/search/_BaseGroupMarkerLayoutView',
    'collections/BillboardList',
    'views/search/BillboardCompositeView'

], function (Marionette, templates, BaseGroupMarkerLayoutView, BillboardList, BillboardCompositeView) {
    'use strict';

    return BaseGroupMarkerLayoutView.extend({
        tagName: 'li',

        ui: function(){
            return _.extend({}, _.result(BaseGroupMarkerLayoutView.prototype, 'ui') || {}, {
                viewGallery: '.view-gallery',
            });
        },

        events: function(){
            return _.extend({}, _.result(BaseGroupMarkerLayoutView.prototype, 'events') || {}, {
                'click @ui.viewGallery' : 'viewGallery',
            });
        },

        template: templates.billboardGroupMarkerLayoutView,

        initialize: function (options) {
            BaseGroupMarkerLayoutView.prototype.initialize.call(this, options);
            this.collection = new BillboardList();
            this.collection.fetch({data:{group_id: this.model.id}});
            this.ListInGroupView = BillboardCompositeView;
        },

        onShow: function(event) {
            BaseGroupMarkerLayoutView.prototype.onShow.call(this, event);
            
            if (this.modelId == this.model.id) {
                this.selectGroup();
                if (this.queryParams.gallery) {
                    this.panGroupWithOffsetOnMap();
                } else {
                    this.panGroupOnMap();
                }
            } else {
                //this.deSelectGroup();
            }
        },

        viewGallery: function(event) {
            app.vent.trigger("goTo", this.url, this.model.id, {gallery: 1});
        }

    });
});