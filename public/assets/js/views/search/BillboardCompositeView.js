/*global define */
define([
    'views/search/_BaseCompositeView',
    'templates',
    'views/search/BillboardMarkerLayoutView'
], function (BaseCompositeView,  templates, BillboardMarkerLayoutView) {
    'use strict';

    return BaseCompositeView.extend({
        template: templates.billboardCompositeView,
        childView: BillboardMarkerLayoutView,
        childViewContainer : "#billboard-list",

        initialize: function (options) {
            BaseCompositeView.prototype.initialize.call(this, options);
            this.bindUIElements();
        }

    });

});