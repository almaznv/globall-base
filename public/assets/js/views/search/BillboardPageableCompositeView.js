/*global define */
define([
    'views/search/_BasePageableCompositeView',
    'templates',
    'views/search/BillboardMarkerLayoutView'
], function (BasePageableCompositeView,  templates, BillboardMarkerLayoutView) {
    'use strict';

    return BasePageableCompositeView.extend({
        template: templates.billboardPageableCompositeView,
        childView: BillboardMarkerLayoutView,
        childViewContainer : "#billboard-list",

        /*ui: function(){
            return _.extend({}, _.result(BaseItemWithMarker.prototype, 'ui') || {}, {
              //'click' : 'onclickChild'
            });
        },

        events: function(){
            return _.extend({}, _.result(BaseItemWithMarker.prototype, 'events') || {}, {
              //'click' : 'onclickChild'
            });
        },

        modelEvents: function(){
            return _.extend({}, _.result(BaseItemWithMarker.prototype, 'modelEvents') || {}, {
              //'click' : 'onclickChild'
            });
        },

        collectionEvents: function(){
            return _.extend({}, _.result(BaseItemWithMarker.prototype, 'collectionEvents') || {}, {
              //'click' : 'onclickChild'
            });
        },

        behaviors: function(){
            return _.extend({}, _.result(BaseItemWithMarker.prototype, 'behaviors') || {}, {

            });
        },*/

        initialize: function (options) {
            BasePageableCompositeView.prototype.initialize.call(this, options);
            this.bindUIElements();
        }

    });

});