/*global define */
define([
    'marionette'
], function (Marionette) {
    'use strict';

    return Marionette.LayoutView.extend({

        initialize: function(options) {
            _.extend(this, options);
            this.queryParams = this.queryParams || {};
            this.link = this.getLink.call(this);
        },

        getLink: function() {
            return {
                url: this.url,
                id: this.model.id,
                queryParams : this.queryParams
            };
        }
        

    });
});