/*global define */
define(['app',
    'marionette',
    'templates',
    'behaviors/Marker',
    'views/search/_BaseLayoutView'

], function (app, Marionette, templates, Marker, BaseLayoutView) {
    'use strict';

    return BaseLayoutView.extend({

        ui: {
            head: '.head',
            item: '.result',
        },

        events: {
            "click @ui.head": "onClick",
            "mouseover @ui.item": "onMouseOver",
            "mouseout @ui.item": "onMouseOut"
        },

        modelEvents: {
            "change:selected": "setSelected",
        },

        behaviors: {
            Marker: {
                behaviorClass: Marker
            }
        },

        initialize: function(options) {
            BaseLayoutView.prototype.initialize.call(this, options);
        },

        setSelected: function () {

            if ( this.model.isSelected() ) { 
                this.triggerMethod("Selected");
            } else {
                this.triggerMethod("DeSelected");
            }
        },

        onClick: function () {
            if ( this.model.isSelected() ) { 
                this.model.deselect();
            } else {
                this.model.setSelected();
            }
        },

        onMouseOver: function () {
            this.triggerMethod("ViewMouseOver");
        },

        onMouseOut: function () {
            this.triggerMethod("ViewMouseOut");
        },

        panMarkerOnMap: function() {
            if (!app.map) return;
            app.map.vent.trigger("panMarker", this.model.get("location"));
        },

        panMarkerWithOffsetOnMap: function() {
            if (!app.map) return; 
            app.map.vent.trigger("panMarker", this.model.get("location"), true);
        }
    });
});