/*global define */
define([
    'views/search/_BaseCompositeView'
], function (BaseCompositeView) {
    'use strict';

    return BaseCompositeView.extend({

        ui: {
            results_info: ".results_info .info",
            previous: ".results_info .previous",
            next: ".results_info .next"
        },

        events: {
            'click @ui.previous' : 'getPreviousPage',
            'click @ui.next' : 'getNextPage'
        },

        collectionEvents: {
            "pageChanged": "updateInfo"
        },

        initialize: function(options) {
            BaseCompositeView.prototype.initialize.call(this, options);
            this.page = (this.queryParams) ? (parseInt(this.queryParams.page) || 1) : 1;

            //TODO фильтрация
            this.collection.getPage(this.page).fetch({data: this.queryParams});
        },

        updateInfo: function(model) {
            var state = this.collection.state;
            var text = "Текущая страница: " + state.currentPage + "<br>";
                text += "из: " + state.totalPages + "<br>";
            this.ui.results_info.html(text);
        },

        getPreviousPage: function(event) {
            if ( this.collection.hasPreviousPage() ) {
                app.vent.trigger("goTo", this.url, null, _.extend(this.queryParams, {page : this.collection.state.currentPage - 1}));
                this.collection.trigger("pageChanged");
            }
        },

        getNextPage: function(event) {
            if ( this.collection.hasNextPage() ) {
                app.vent.trigger("goTo", this.url, null, _.extend(this.queryParams, {page : this.collection.state.currentPage + 1}));
                this.collection.trigger("pageChanged");
            }
        },

        onShow: function(event) {
            BaseCompositeView.prototype.onShow.call(this, event);
            this.collection.trigger("pageChanged");
        }

    });
});