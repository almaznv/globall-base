/*global define */
define([
    'marionette',
    'templates',
    'views/search/_BaseItemView',
    'models/Favourite',
    'models/Subscribe'

], function (Marionette, templates, BaseItemView, FavouriteModel, SubscribeModel) {
    'use strict';

    return BaseItemView.extend({
        template: templates.billboardSideItemView,

        ui: {
            addfavourite: ".add-to-favorite",
            addsubscribe: ".add-to-subscribe",
            delfavourite: ".delete-from-favorite",
            delsubscribe: ".delete-from-subscribe",
        },

        events: {
            "click @ui.addfavourite" : function () {
                this.favouriteModel.save({wait: true});
                this.model.set("favourite", this.favouriteModel.id);
                this.model.save();
                this.render();
            },
            "click @ui.delfavourite" : function () {
                this.favouriteModel.destroy({wait: true});
                this.model.set("favourite", undefined);
                this.model.save();
                this.render();
            },
            "click @ui.addsubscribe" : function () {
                this.subscribeModel.save({wait: true});
                this.model.set("subscribe", this.subscribeModel.id);
                this.model.save();
                this.render();
            },
            "click @ui.delsubscribe" : function () {
                this.subscribeModel.destroy({wait: true});
                this.model.set("subscribe", undefined);
                this.model.save();
                this.render();
            }
        },

        templateHelpers: function() {
            var s = this;
            return {
                isInFavourites: function() {
                    return s.model.get("favourite");
                },
                isInSubscribes: function() {
                    return s.model.get("subscribe");
                }
            };
        },

        initialize: function (options) {
            this.favouriteModel = new FavouriteModel({ id:this.model.favourite, side_id: this.model.id});
            this.subscribeModel = new SubscribeModel({ id:this.model.subscribe, side_id: this.model.id});
        }
    });
});