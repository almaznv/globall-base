/*global define */
define([
    'underscore','marionette', 'lib/goog!maps,3'
], function (_, Marionette) {
    'use strict';

    var rememberLastLocation = function(lat, lon) {
        localStorage.setItem("lastLocation", JSON.stringify({lat:lat,lon:lon}));
    };

    return Marionette.ItemView.extend({
        el:"#m_map",
        markers: [],
        initialize: function(options){
            var location, s = this;
            _.extend(this, options);
            location = this.location;

            this.map = new google.maps.Map(this.el,{
                zoom: 12,
                center: new google.maps.LatLng(location.lat, location.lon),
                mapTypeControl: true,
                mapTypeControlOptions: {
                  position: google.maps.MapTypeControlStyle.TOP_RIGHT,
                  style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                },
                zoomControl: true,
                zoomControlOptions: {
                  style: google.maps.ZoomControlStyle.DEFAULT,
                  position: google.maps.ControlPosition.RIGHT_BOTTOM
                }
            });

            google.maps.event.addListener(this.map, 'idle', function() {
                var center = s.map.getCenter();
                rememberLastLocation(center.lat(), center.lng());

                //запускаем отложенные события для карты
                if (!s.mapLoaded) {
                    s.mapDelayedEventsCollection.each( function(item){
                        var func = item.get("func"), 
                            args = item.get("args");
                        func.apply(s, args);
                    });
                    s.mapDelayedEventsCollection.reset();
                }
                s.mapLoaded = 1;
            });


            return this;
        },

        setCenter: function(lat, lon){
            var latLng = new google.maps.LatLng(lat, lon);
            this.map.setCenter(latLng);
        },

        setMarker: function (options) {
            var markerIcon, lat, lon, latLng, marker, onClick, onMouseOver, onMouseOut;

            markerIcon = options.markerIcon;
            lat = options.lat;
            lon = options.lon;
            onClick = options.onClick;
            onMouseOver = options.onMouseOver;
            onMouseOut = options.onMouseOut;

            latLng = new google.maps.LatLng(lat, lon);

            marker = new google.maps.Marker({
                icon: markerIcon,
                position: latLng,
                map: this.map
            });

            if (onClick) {
                google.maps.event.addListener(marker, 'click', onClick);
            }

            if (onMouseOver) {
                google.maps.event.addListener(marker, 'mouseover', onMouseOver);
            }

            if (onMouseOut) {
                google.maps.event.addListener(marker, 'mouseout', onMouseOut);
            }

            return marker;
        },

        panToMarker: function (location) {
            var latLng = new google.maps.LatLng(location.lat, location.lon);
            this.map.panTo(latLng);
        },

        panToGroupMarkers: function (locations, withOffset) {
            var bounds = new google.maps.LatLngBounds();
            _.each(locations, function (location) {
                bounds.extend( new google.maps.LatLng(location.lat, location.lon) );
            });

            this.map.fitBounds(bounds);
            if (withOffset) {
                this.map.panToWithOffset(bounds.getCenter());
            } else {
                this.map.panTo(bounds.getCenter());
            }
            
            this.map.setZoom(this.map.getZoom()-2);
        }
    });

});