/*global define */
define([
	'marionette',
	'templates'
], function (Marionette, templates) {
	'use strict';

	return Marionette.CompositeView.extend({
		tagName: 'li',

		template: templates.newsItemView,
	});
});