/*global define */
define([
    'marionette',
    'templates',
    'views/index/NewsItemView'
], function (Marionette, templates, NewsItemView) {
    'use strict';

    return Marionette.CompositeView.extend({
        template: templates.newsListView,

        childView: NewsItemView,

        childViewContainer: '#news-list',

        initialize: function(options) {
            this.collection.fetch();
        }
    });

});