/*global define */
define([
    'marionette'
], function (Marionette) {
    'use strict';

    return Marionette.ItemView.extend({
        el:"#m_navbar"
    });

});